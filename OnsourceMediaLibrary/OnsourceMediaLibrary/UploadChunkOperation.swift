//
//  GetDataOperation.swift
//  SmartImageUploader
//
//  Created by Girdhar on 08/02/18.
//  Copyright © 2018 Girdhar. All rights reserved.
//

import Foundation
class UploadChunkOperation: OnsourceOperation {
    
    var chunk: Data?
    var chunkId: NSNumber
    var fileId: NSNumber
    var isCompleted: String
    var serviceRequestId: String
    var inspectorId: String
    var userId: String
    var filename: String
    var fileType:String
    var totalChunkCount: String
    var responseData: Data?
    var containerName: String
    var resDescription: String
    var claimId: String
    var resTimeStamp: String
    var geolocation: String
    var notes: String
    var textLog: OSMLLog
    
    init(withMedia dataChunk: Data?, chunkId:NSNumber, fileId:NSNumber, isCompleted:String, serviceRequestId: String, inspectorId:String, userId:String, filename:String, fileType:String, totalChunkCount:String, containerName:String, resDescription:String, claimId:String, resourceTimeStamp: String, geolocation: String, notes: String) {
        
        self.chunk = dataChunk
        self.chunkId = chunkId
        self.fileId = fileId
        self.isCompleted = isCompleted
        self.serviceRequestId = serviceRequestId
        self.inspectorId = inspectorId
        self.userId = userId
        //self.filename = filename
        self.fileType = fileType
        self.totalChunkCount = totalChunkCount
        self.containerName = containerName
        self.resDescription = resDescription
        let result = filename.split(separator: "/")
        if let lastString = result.last {
            self.filename = String(lastString)
        }
        else {
            self.filename = filename
        }
        self.claimId = claimId
        self.resTimeStamp = resourceTimeStamp
        self.geolocation = geolocation
        self.notes = notes
        self.textLog = OSMLLog(withLogFile: serviceRequestId)
    }
    
    override func main() {
        guard isCancelled == false else {
            finish(true)
            return
        }
        executing(true)
        uploadMediaChunk()
    }
    
    
    func uploadMediaChunk() {
        
        let parameters = ["resourceName":self.filename, "fileType": self.fileType]
        guard let url = URL(string: kchunkUploadURL) else { return }
        var request = URLRequest(url: url)
        request.httpMethod = kHttpMethodPost
        request.timeoutInterval = 1200
        let boundary = generateBoundary()
        request.addValue("multipart/form-data; boundary=\(boundary)", forHTTPHeaderField: "Content-Type")
        
        request.addValue(kchunkUploadKeyValue, forHTTPHeaderField: ktokenName)
        request.addValue(self.filename, forHTTPHeaderField:kFileName)
        request.addValue(self.userId, forHTTPHeaderField:kUserId)
        request.addValue(String(describing:self.inspectorId), forHTTPHeaderField:kInspectorId)
        request.addValue(String(describing:self.serviceRequestId), forHTTPHeaderField: kServiceRequestId)
        request.addValue(String(describing:self.fileId), forHTTPHeaderField: kFileId)
        request.addValue(String(describing:self.chunkId), forHTTPHeaderField:kChunkId)
        request.addValue(self.isCompleted, forHTTPHeaderField:kIsCompleted)
        request.addValue(self.totalChunkCount, forHTTPHeaderField: kTotalaChunkCount)
        request.addValue(self.fileType, forHTTPHeaderField: kMediaType)
        request.addValue(self.containerName, forHTTPHeaderField: kContainerName)
        request.addValue(kApplicationId, forHTTPHeaderField: kAppId)
        request.addValue(self.resDescription, forHTTPHeaderField: kDescription)
        request.addValue(self.claimId, forHTTPHeaderField: kClaimId)
        
        request.addValue(self.resTimeStamp, forHTTPHeaderField: kTimeStamp)
        request.addValue(self.geolocation, forHTTPHeaderField: kGeoCode)
        request.addValue(self.notes, forHTTPHeaderField: kNotes)
        
        let dataBody = createDataBody(withParameters: parameters, media: self.chunk, boundary: boundary)
        request.httpBody = dataBody
        //logging
        textLog.write(String(format: "Chunk request : %@ \n", request.allHTTPHeaderFields!))
        //
        let session = URLSession(configuration: .default)
//        print("request:---------------", request.allHTTPHeaderFields!)
        session.dataTask(with: request) {(data, response, error) in
            if error == nil {
                if data != nil {
                    let responsString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Chunk Upload Response Data:", responsString!)
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            self.responseData = data
                        }
                        else {
                            //print("Response Status Code != 200")
                            self.responseData = nil
                        }
                    }
                    else {
                        self.responseData = nil
                    }
                }
                else {
                    //print("Chunk Upload Response Data is Nil...........")
                    self.responseData = nil
                }
            }
            else {
                self.responseData = nil
            }
            self.executing(false)
            self.finish(true)
            
        }.resume()
    }
    
    func createDataBody(withParameters params: Parameters?, media: Data?, boundary: String) -> Data {
        let lineBreak = "\r\n"
        var body = Data()
        body.append("--\(boundary + lineBreak)")
        body.append("Content-Disposition: form-data; name=\"image\"; filename=\(self.filename)\(lineBreak)")
        body.append("Content-Type:image/jpeg\(lineBreak + lineBreak)")
        if let chunkData = media {
            body.append(chunkData)
            body.append(lineBreak)
            body.append("--\(boundary)--\(lineBreak)")
        }
        else {
            body.append("")
            body.append(lineBreak)
            body.append("--\(boundary)--\(lineBreak)")
        }
        return body
    }
    
    func generateBoundary() -> String {
        return "Boundary-\(UUID().uuidString)"
    }
}
