//
//  ResourceEntity.swift
//  OnsourceMediaLibrary
//
//  Created by Ankit Jain on 09/02/18.
//  Copyright © 2018 Ankit Jain. All rights reserved.
//

import Foundation
import CoreData

class ResourceMO: NSManagedObject {
    
    @NSManaged var userId: String?
    @NSManaged var inspectorId: String?
    @NSManaged var serviceRequestId: String?
    @NSManaged var resourceId: NSNumber?
    @NSManaged var path: String?
    @NSManaged var type: String?
    @NSManaged var uploadStatus: String?
    @NSManaged var url: String?
    @NSManaged var resDescription: String?
    @NSManaged var containerName: String?
    @NSManaged var timeStarted: NSDate?
    @NSManaged var claimId: String?
    @NSManaged var dataLength: NSNumber?
    @NSManaged var dataRemaining: NSNumber?
    @NSManaged var resourceTimeStamp: String?
    @NSManaged var geolocation: String?
    @NSManaged var notes: String?
    
}

class ChunkMO: NSManagedObject {
    
    @NSManaged var timeStarted: NSDate?
    @NSManaged var chunkId: NSNumber?
    @NSManaged var resourceId: NSNumber?
    @NSManaged var uploadStatus: String?
    @NSManaged var chunkSize: NSNumber?
}
