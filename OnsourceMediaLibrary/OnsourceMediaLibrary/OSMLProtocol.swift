//
//  OSMLProtocol.swift
//  OnsourceMediaLibrary
//
//  Created by Ankit Jain on 26/02/18.
//  Copyright © 2018 Ankit Jain. All rights reserved.
//

import Foundation

@objc protocol OSMLDelegate: class {
    func uploadCompletedForRecord(serviceRequestId: String, inspectorId: String, status:Bool)
    func uploadCompletedForResource(serviceRequestId: String, inspectorId: String, resourceUrl: String)
    func assessmentCompletedForRecord(serviceRequestId: String, inspectorId: String, status:Bool)
    func bundlingCompletedForRecord(serviceRequestId: String, inspectorId: String, status:Bool)
    func mediaLibraryNetworkErrorHandler()
    func uploadProgressForRecord(serviceRequestId: String, inspectorId: String, progressPercentage: Int)
}

protocol OSMLNetworkDelegate: class {
    func uploadCompletedForRecord(serviceRequestId: String, inspectorId: String, status:Bool)
    func uploadCompletedForResource(serviceRequestId: String, inspectorId: String, resourceId: String)
    func assessmentCompletedForRecord(serviceRequestId: String, inspectorId: String, status:Bool)
    func bundlingCompletedForRecord(serviceRequestId: String, inspectorId: String, status:Bool)
    func uploadProgressForRecord(serviceRequestId: String, inspectorId: String, progressPercentage: Int)
}

