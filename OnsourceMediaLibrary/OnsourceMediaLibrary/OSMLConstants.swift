//
//  OnsourceMediaLibraryConstants.swift
//  OnsourceMediaLibrary
//
//  Created by Ankit Jain on 13/02/18.
//  Copyright © 2018 Ankit Jain. All rights reserved.
//
import Foundation

typealias Parameters  = [String: String]

enum UploadStatus: String {
    case Started = "started"
    case InProgress = "in progress"
    case Completed = "completed"
}

enum SignalStrength: Int32 {
    case Poor = 1
    case Weak = 2
    case Normal = 3
    case Strong = 4
}

let kServiceRequestId = "SrvReqId"
let kChunkId = "ChunkId"
let kUserId = "UserId"
let kFileName = "FileName"
let kFileId = "FileId"
let kBlobUri = "BlobUri"
let kInspectorId = "InspId"
let kMediaPath = "MdPath"
let kMediaType = "MdType"
let kMedia = "media"
let kIsSuccess = "IsSuccess"
let kIsCompleted = "IsCompleted"
let kSuccess = "Success"
let kContainerName = "CntName"
let kTotalaChunkCount = "TtlChnkCnt"
let kAppId = "AppId"
let kMeidaCount = "MdCnt"
let kDescription = "descp"
let kClaimId = "ClaimId"
let kInspemailid = "inspemailid"
let kContainer_Name = "AR Property Inspection";
let kTimeStamp = "TimeStamp"
let kGeoCode = "GeoCode"
let kNotes = "DescOfLoss"

//let kApplicationId = Bundle.main.bundleIdentifier
let kApplicationId = "com.onsrcmedlib.genpact"
let kHttpMethodPost = "POST"
let kChunkSize = 409600

/*
 //Development Environment
 //Upload Chunk
 let kchunkUploadURL = "https://onsourceappservice.azurewebsites.net/api/uploadmedia"
 let kchunkUploadKeyValue = "pg0IhzkFlvkxLkJGYFbaLRmRU8kW6KwI6nSKYaR7GW5o/Et4MC0FKg=="
 
 //Process Image
 let kresourceUploadURL = "https://onsourcemedia-east.azurewebsites.net/api/processmedia"
 let kprocessImageKeyValue = "3yHKDTF4O1QpppoGf9zcIrWG2JeuLFR16uOAyqv8QsPQUSr7ha6kPg=="
 
 //Bundling Image
 let kbundlingResourceURL = "https://onsourceappservice.azurewebsites.net/api/bundleImage"
 let kbundlingImageKeyValue = "HXFJjzzYbMLd4EroCQhdyUQBsEMmbxS7xuEdAxqNyXDQvuSH2T3SkA=="
 */


//Production Environment
//Upload Chunk
let kchunkUploadURL = "https://media.onsourceonline.com/api/uploadmedia"
let kchunkUploadKeyValue = "EeYnGBYLHMCeLhPFZsROQCrLTZPm2wtEB3ho1FTqMzp5N7YgHrXpNw=="

//Process Image
let kresourceUploadURL = "https://media.onsourceonline.com/api/processmedia"
let kprocessImageKeyValue = "3yHKDTF4O1QpppoGf9zcIrWG2JeuLFR16uOAyqv8QsPQUSr7ha6kPg=="

//Bundling Image
let kbundlingResourceURL = "https://media.onsourceonline.com/api/bundlemedia"
let kbundlingImageKeyValue = "2kt1Rwl6x80o2SXakbn/XaCqv2LxmGAClghkOK8iB7/DTGJmMRqiFQ=="


let ktokenName = "x-functions-key"


let kChunkIndex = 88888888
let krequestTotalCount = 3
let kmaxConcurrentOperationCount = 10

public class CheckNetworkConnectivity {
    func isNetworkAvailable() -> Int {
        let reachability = Reachability()
        var networkType = 0
        if reachability?.connection == .wifi {
            networkType = 1
        } else if reachability?.connection == .cellular {
            networkType = 2
        } else {
            networkType = 0
        }
        return networkType
    }
}

public class CommonMethods {
    func pathForResource(withRelativePath relativePath:String) -> URL? {
        /*
         Document directory
 
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let path = documentsDirectory.appendingPathComponent(relativePath)
        */
        
        /*
         Main Bundle
         */
         guard let path = Bundle.main.url(forResource: relativePath, withExtension: nil) else { return nil}
        
      return path
    }
}



