//
//  OSMLUploader.swift
//  OnsourceMediaLibrary
//
//  Created by Ankit Jain on 16/02/18.
//  Copyright © 2018 Ankit Jain. All rights reserved.
//

import Foundation
import UIKit

@objc open class ResourceUploader: NSObject,OSMLNetworkDelegate {
    
    @objc var delegate: OSMLDelegate?
    var requestCountFlag = 0
    
    @objc func prepareAndStartUpload(resources: NSDictionary, viewController:UIViewController) {
        self.cleanupChunksIfExceededTime()
        switch CheckNetworkConnectivity().isNetworkAvailable() {
        case 1:
            self.startUpload(withResources: resources)
        case 2:
//            if getSignalStrength() < SignalStrength.Normal.rawValue {
//                let alert = UIAlertController(title: "Alert!", message: "Network connectivity is very low. Do you still want to continue?", preferredStyle: UIAlertControllerStyle.alert)
//                alert.addAction(UIAlertAction(title: "Continue", style: UIAlertActionStyle.default, handler: {action in
//                    self.startUpload(withResources: resources)
//                }))
//                alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: nil))
//                viewController.present(alert, animated: true, completion: nil)
//            }
//            else {
//                self.startUpload(withResources: resources)
//            }
            self.startUpload(withResources: resources)
        default:
            let alert = UIAlertController(title: "Alert!", message: "Network not found please try after some time.", preferredStyle: UIAlertControllerStyle.alert)
            alert.addAction(UIAlertAction(title: "Cancel", style: UIAlertActionStyle.cancel, handler: {action in
                self.delegate?.mediaLibraryNetworkErrorHandler()
            }))
            viewController.present(alert, animated: true, completion: nil)
        }
        
    }
    
    func startUpload(withResources resources:NSDictionary) {
        let dataController: CoreDataController = CoreDataController.shared
        let mediaResources:[NSDictionary] = resources.value(forKey: "MediaResource") as! [NSDictionary]
        
        //logging
        var textLog = OSMLLog(withLogFile: resources.value(forKey: "ServiceRequestId") as! String)
        textLog.write(String(format: "Started: %@ \n Media Count to upload: %d \n Reource Dict Received To Upload : \n %@ \n", NSDate().description, (resources.value(forKey: "MediaResource") as! [NSDictionary]).count, resources.description))
        //
        
        //fetch resource corresponding to the serviceRequestId and inspectorId
        let resourcesMO = dataController.fetchResourcesForARecord(serviceRequestId: resources.value(forKey: "ServiceRequestId") as! String, inspectorId: resources.value(forKey: "InspectorId") as! String)
        if resourcesMO.count == 0 {
            let currentTime: NSDate = NSDate()
//            print("Current Timestamp1 : ", currentTime)//no records exist
            for resource in mediaResources {
                if ((resource.value(forKey: "filePath") as! String) != "") {    //if filepath is not blank (check to handle error of input data)
                    let resourceDataLen = resourceDataLength(resource.value(forKey: "filePath") as! String)
                    dataController.saveResource(path: resource.value(forKey: "filePath") as! String, status: UploadStatus.Started.rawValue, serviceRequestId: resources.value(forKey: "ServiceRequestId") as! String, inspectorId: resources.value(forKey: "InspectorId") as! String, type: resource.value(forKey: "fileType") as! String, userId: resources.value(forKey: "UserId") as! String, url: "", resDescription: resource.value(forKey: "description") as! String, containerName:resource.value(forKey: "ContainerName") as! String, timeStarted: currentTime, claimId: resources.value(forKey: "claimId") as! String, resourceTimeStamp: resource.value(forKey: "resourceTimeStamp") as! String, geolocation: resource.value(forKey: "geolocation") as! String,notes: resource.value(forKey: "scopeofwork") as! String, dataLength:NSNumber(value: resourceDataLen), dataRemaining:NSNumber(value: resourceDataLen))
                }
            }
            DispatchQueue.global(qos: .background).async {
                let onsourceNetwork = OnsourceNetworkUtility(withServiceRequestId: resources.value(forKey: "ServiceRequestId") as! String, inspectorId: resources.value(forKey: "InspectorId") as! String)
                onsourceNetwork?.delegate = self
                onsourceNetwork?.uploadResources()
            }
        } else {
//            self.delegate?.uploadCompletedForRecord(serviceRequestId: resources.value(forKey: "ServiceRequestId") as! String, inspectorId: resources.value(forKey: "InspectorId") as! String, status: false)
            self.resumeUpload(withserviceRequestId: resources.value(forKey: "ServiceRequestId") as! String, inspectorId: resources.value(forKey: "InspectorId") as! String)
        }
    }
    
    @objc func startAssessment(serviceRequestId: String, inspectorId: String) {
        //logging
        var textLog = OSMLLog(withLogFile: serviceRequestId)
        textLog.write("Assessment started \n")
        //
        let dataController: CoreDataController = CoreDataController.shared
        let resources = dataController.fetchResourcesWithURLForARecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId)
        if resources.count != 0 {
            DispatchQueue.global(qos: .background).async {
                let onsourceNetwork = OnsourceNetworkUtility(withServiceRequestId: serviceRequestId, inspectorId: inspectorId)
                onsourceNetwork?.delegate = self
                onsourceNetwork?.assessResources(resources: resources)
            }
        }
        else {
            self.delegate?.assessmentCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
        }
    }
    @objc func startBundling(withserviceRequestId serviceRequestId: String, inspectorId: String) {
        //logging
        var textLog = OSMLLog(withLogFile: serviceRequestId)
        textLog.write("Bundling started \n")
        //
        let dataController: CoreDataController = CoreDataController.shared
        let resources = dataController.fetchResourcesWithURLForARecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId)
        if resources.count > 0 {
            DispatchQueue.global(qos: .background).async {
                let onsourceNetwork = OnsourceNetworkUtility(withServiceRequestId: serviceRequestId, inspectorId: inspectorId)
                onsourceNetwork?.delegate = self
                onsourceNetwork?.bundlingResource(withserviceRequestId: serviceRequestId, inspectorId: inspectorId, mediaCount: resources.count)
            }
        } else {
            self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
        }
    }
    
    func resetUpload(serviceRequestId:String, inspectorId:String) {
        //logging
        var textLog = OSMLLog(withLogFile: serviceRequestId)
        textLog.write("Reset Upload \n")
        //
        let dataController: CoreDataController = CoreDataController.shared
        dataController.deleteResources(serviceRequestId: serviceRequestId, inspectorId: inspectorId)
    }
    
    @objc func resumeUpload(withserviceRequestId serviceRequestId: String, inspectorId: String) {       //called when application is live again
        DispatchQueue.global(qos: .background).async {
            let onsourceNetwork = OnsourceNetworkUtility(withServiceRequestId: serviceRequestId, inspectorId: inspectorId)
            onsourceNetwork?.delegate = self
            onsourceNetwork?.uploadResources()
        }
    }
    
    func uploadLogToServer(forSRId srId: String, inspectorId inspId: String, deleteWhenDone:Bool) {
        let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first!
        let path = documentsDirectory.appendingPathComponent(String(format: "%@.txt", srId))
//        guard let path = CommonMethods().pathForResource(withRelativePath: String(format: "%@.txt", srId)) else { return }
        var data:Data = Data()
        do
        {
            data = try Data(contentsOf: path)
        }
        catch let error
        {
            print(error.localizedDescription)
        }
        let operationQueue: OperationQueue = OperationQueue()
        let uploadOperation = UploadChunkOperation(withMedia: data, chunkId: 22222, fileId: 999999, isCompleted: "false", serviceRequestId: srId, inspectorId: inspId, userId: "", filename: String(format: "%@.txt", srId), fileType:"metadata", totalChunkCount:"1", containerName:kContainer_Name, resDescription:"", claimId:"", resourceTimeStamp: "", geolocation: "", notes: "")
        uploadOperation.completionBlock = {
            if uploadOperation.responseData != nil {
                do {
                    let json:NSDictionary = try JSONSerialization.jsonObject(with: uploadOperation.responseData!, options: []) as! NSDictionary
                    print("Log file uploaded : \n",json)
                    if(deleteWhenDone) {
                        self.removeLog(forPath:String(format: "%@.txt", srId))
                    }
                }
                catch let error {
                    print(error.localizedDescription)
                }
            }
        }
        operationQueue.addOperations([uploadOperation], waitUntilFinished: true)
    }
    
    func removeLog(forPath path:String) {
        let fileManager = FileManager.default
        let nsDocumentDirectory = FileManager.SearchPathDirectory.documentDirectory
        let nsUserDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(nsDocumentDirectory, nsUserDomainMask, true)
        guard let dirPath = paths.first else {
            return
        }
        let filePath = "\(dirPath)/\(path)"
        do {
            try fileManager.removeItem(atPath: filePath)
        } catch let error as NSError {
            print(error.debugDescription)
        }
    }
    
    func cleanupChunksIfExceededTime() {
        let dataController: CoreDataController = CoreDataController.shared
        let calendar = Calendar.current
        let expiryTime: NSDate = calendar.date(byAdding: .minute, value: 5, to: Date())! as NSDate
        dataController.deleteExpiredChunks(expiryTime: expiryTime)
    }
    
    func getSignalStrength() -> Int32 {
        let application = UIApplication.shared
        let statusBarView = application.value(forKey: "statusBar") as! UIView
        let foregroundView = statusBarView.value(forKey: "foregroundView") as! UIView
        let foregroundViewSubviews = foregroundView.subviews
        var dataNetworkItemView:UIView!
        for subview in foregroundViewSubviews {
            if subview.isKind(of: NSClassFromString("UIStatusBarSignalStrengthItemView")!) {
                dataNetworkItemView = subview
                break
            } else {
                return 0
            }
        }
        return dataNetworkItemView.value(forKey: "signalStrengthBars") as! Int32
    }
    
    func resourceDataLength(_ resourcePath: String) -> Int {
//        guard let path = Bundle.main.url(forResource: resourcePath, withExtension: nil) else { return 0}
        guard let path = CommonMethods().pathForResource(withRelativePath: resourcePath) else { return 0}
        //let path = URL(fileURLWithPath: resource.path!)
        var resourceDataLen:Int = 0
        do
        {
            let data = try Data(contentsOf: path)
            resourceDataLen = (data as NSData).length
        }
        catch let error
        {
            print(error.localizedDescription)
        }
        return resourceDataLen;
    }
    
    func uploadCompletedForRecord(serviceRequestId: String, inspectorId: String, status:Bool) {
        if status {
            DispatchQueue.main.async {
                self.delegate?.uploadCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: status)
            }
        }
        else {
            if requestCountFlag < krequestTotalCount {
                requestCountFlag += 1
                self.resumeUpload(withserviceRequestId: serviceRequestId, inspectorId: inspectorId)
            }
            else {
                DispatchQueue.main.async {
                    self.delegate?.uploadCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: status)
                    self.uploadLogToServer(forSRId: serviceRequestId, inspectorId: inspectorId, deleteWhenDone: false)
                }
                
            }
        }
    }
    
    func uploadCompletedForResource(serviceRequestId: String, inspectorId: String, resourceId: String) {
//        let dataController: CoreDataController = CoreDataController.shared
//        let resourceObj:ResourceMO = dataController.fetchResourceDetail(resourceId: resourceId)
        DispatchQueue.main.async {
            self.delegate?.uploadCompletedForResource(serviceRequestId: serviceRequestId, inspectorId: inspectorId, resourceUrl: resourceId)
        }
        
    }
    
    func uploadProgressForRecord(serviceRequestId: String, inspectorId: String, progressPercentage: Int) {
        print("Progress percentage : ", progressPercentage)
        DispatchQueue.main.async {
            self.delegate?.uploadProgressForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, progressPercentage: progressPercentage)
        }
    }
    
    func assessmentCompletedForRecord(serviceRequestId: String, inspectorId: String, status:Bool) {
        print("Process Image Status:", status)
        if(!status) {
            self.uploadLogToServer(forSRId: serviceRequestId, inspectorId: inspectorId, deleteWhenDone: false)
        }
        DispatchQueue.main.async {
            self.delegate?.assessmentCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: status)
        }
        
    }
    
    func bundlingCompletedForRecord(serviceRequestId: String, inspectorId: String, status:Bool) {
        print("bundling Completed For Record Status:", status)
        if status {
            let dataController: CoreDataController = CoreDataController.shared
            dataController.deleteResources(serviceRequestId: serviceRequestId, inspectorId: inspectorId)
            self.uploadLogToServer(forSRId: serviceRequestId, inspectorId: inspectorId, deleteWhenDone:true)
        }
        else {
            self.uploadLogToServer(forSRId: serviceRequestId, inspectorId: inspectorId, deleteWhenDone:false)
        }
        
        DispatchQueue.main.async {
            self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: status)
        }
    }
    
}
