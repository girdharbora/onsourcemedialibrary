//
//  OnsourceNetworkUtility.swift
//  SmartImageUploader
//
//  Created by Girdhar on 08/02/18.
//  Copyright © 2018 Girdhar. All rights reserved.
//

import Foundation
import UIKit
import CoreTelephony

class OnsourceNetworkUtility {
    var serviceCompletedFlag = true
    var totalOprationCount = 0
    private let operationQueue: OperationQueue = OperationQueue()
    
    private let secondOperationQueue: OperationQueue = OperationQueue()
    let dataController:CoreDataController
    var mediaResources:[ResourceMO]
    var delegate: OSMLNetworkDelegate?
    var serviceRequestId: String
    var inspectorId: String
    var textLog: OSMLLog
    
    typealias RequestCompletionHandler = (_ success:Bool) -> Void
    
    init?(withServiceRequestId:String, inspectorId:String) {
        dataController = CoreDataController.shared
        mediaResources = dataController.fetchResourcesToBeUploaded(serviceRequestId: withServiceRequestId, inspectorId: inspectorId)
        self.serviceRequestId = withServiceRequestId
        self.inspectorId = inspectorId
        textLog = OSMLLog(withLogFile: withServiceRequestId)
    }
    
    func uploadResources() -> Void {
        operationQueue.maxConcurrentOperationCount = kmaxConcurrentOperationCount
        if CheckNetworkConnectivity().isNetworkAvailable() > 0 {
            if mediaResources.count > 0 {   //check whether any of the resource is to be uploaded
                var operationList = [UploadChunkOperation]()
                for resource in mediaResources {
//                    guard let path = Bundle.main.url(forResource: resource.path, withExtension: nil) else { return }
                    guard let path = CommonMethods().pathForResource(withRelativePath: resource.path!) else { return }
                    //let path = URL(fileURLWithPath: resource.path!)
                    //path to be changed
                    let chunks = splitDatainChunks(path)    //split resource data into chunks
                    var chunksTobeUploaded:[ChunkMO] = dataController.fetchChunksToBeUploaded(resourceId: resource.resourceId!)
                    
                    if chunksTobeUploaded.count == 0 {
                        //check whether chunks has been created earlier
                        //print("Total Chunk Count:", chunks.count)
                        let currentTime: NSDate = NSDate()
                        for chunkIndex in (0..<chunks.count) {
                            dataController.saveChunk(resourceId:resource.resourceId!, status: UploadStatus.Started.rawValue, chunkId: NSNumber(value: chunkIndex+kChunkIndex), timeStarted: currentTime, chunkSize:(chunks[chunkIndex] as NSData).length)
                        }
                        chunksTobeUploaded = dataController.fetchChunksToBeUploaded(resourceId: resource.resourceId!)
                    }
                    //logging
                    textLog.write(String(format: "File Path: %@ \n Total chunk count: %d \n Remaining Chunk count: %d \n", resource.path!, chunks.count, chunksTobeUploaded.count))
                    //
                    for chunk in chunksTobeUploaded
                    {
                        let uploadOperation = UploadChunkOperation(withMedia: chunks[((chunk.chunkId?.intValue)!-kChunkIndex)], chunkId: chunk.chunkId!, fileId: resource.resourceId!, isCompleted: "false", serviceRequestId: resource.serviceRequestId!, inspectorId: resource.inspectorId!, userId: resource.userId!, filename: resource.path!, fileType:resource.type!, totalChunkCount:String(chunks.count), containerName:resource.containerName!, resDescription:resource.resDescription!, claimId:resource.claimId!, resourceTimeStamp: resource.resourceTimeStamp!, geolocation: resource.geolocation!, notes: resource.notes!)
                        uploadOperation.completionBlock = {
                            
                            self.totalOprationCount += 1
                            //print("Operation Completion Block Count...............", self.totalOprationCount)
                            if uploadOperation.responseData != nil {
                                do {
                                    let json:NSDictionary = try JSONSerialization.jsonObject(with: uploadOperation.responseData!, options: []) as! NSDictionary
                                    //logging
                                    self.textLog.write(String(format: "Completed Chunk response : %@ \n", json.description))
                                    //
                                    if json[kServiceRequestId] != nil {
                                        let uploadedChunkSize = self.dataController.updateChunkUploadStatus(chunkId: NSNumber(value: Int(json[kChunkId] as! String)!), resourceId: NSNumber(value: Int(json[kFileId] as! String)!), status: UploadStatus.Completed.rawValue)
                                        print(uploadedChunkSize)
                                        let aResource = self.dataController.fetchResourceDetail(resourceId: NSNumber(value: Int(json[kFileId] as! String)!))
                                        let resourceDataRemaining = aResource.dataRemaining as! Int
//                                        print("remaining : ",(resourceDataRemaining-uploadedChunkSize))
                                        self.dataController.updateResourceUploadProgress(resourceId: NSNumber(value: Int(json[kFileId] as! String)!), dataRemaining: NSNumber(value: resourceDataRemaining-uploadedChunkSize))
                                        let chunks:[ChunkMO] = self.dataController.fetchChunksToBeUploaded(resourceId: NSNumber(value: Int(json[kFileId] as! String)!))
                                        let blobURI = json[kBlobUri] as? String
                                        if blobURI != nil {
                                            //logging
                                            self.textLog.write(String(format: "Blob Uri Received : %@ \n", blobURI!))
                                            //
                                            self.dataController.updateResourceURL(resourceId: NSNumber(value: Int(json[kFileId] as! String)!), url: blobURI!)
                                        }
                                        let uploadPercentage = self.uploadPercentageForARecord(serviceRequestId: json[kServiceRequestId] as! String, inspectorId: json[kInspectorId] as! String)
//                                        print("upload percent : ",uploadPercentage)
                                        self.delegate?.uploadProgressForRecord(serviceRequestId: json[kServiceRequestId] as! String, inspectorId: json[kInspectorId] as! String, progressPercentage: uploadPercentage)
                                        if chunks.count == 0 {
                                            //logging
                                            self.textLog.write(String(format: "Completed Resource File Path : %@ \n", json[kFileName] as! String))
                                            //
                                            self.dataController.deleteChunks(resourceId: NSNumber(value: Int(json[kFileId] as! String)!))
                                            let uploadStatusUpdated = self.dataController.updateResourceUploadStatus(resourceId: NSNumber(value: Int(json[kFileId] as! String)!), status: UploadStatus.Completed.rawValue)
                                            if (uploadStatusUpdated) {  //Check if the status has been updated to completed
                                                //logging
                                                self.textLog.write(String(format: "URI Received for Resource File Path : %@ \n", json[kFileName] as! String))
                                                //
                                                //sending message to uploader class about completion of upload
                                                self.delegate?.uploadCompletedForResource(serviceRequestId: json[kServiceRequestId] as! String, inspectorId: json[kInspectorId] as! String, resourceId:json[kFileName] as! String)
                                                
                                                let remainingResources = self.dataController.fetchRemainingResourcesForARecord(serviceRequestId: json[kServiceRequestId] as! String, inspectorId: json[kInspectorId] as! String)
                                                if remainingResources.count == 0 {
                                                    //All resources for a record completed upload
                                                    self.delegate?.uploadCompletedForRecord(serviceRequestId: json[kServiceRequestId] as! String, inspectorId: json[kInspectorId] as! String, status: true)
                                                }
                                            }
                                            else {  //if upload status was not updated due to blank uri
                                                //logging
                                                self.textLog.write(String(format: "URI not Received for Resource File Path : %@ \n", json[kFileName] as! String))
                                                //
                                                self.dataController.updateResourceUploadProgress(resourceId: NSNumber(value: Int(json[kFileId] as! String)!), dataRemaining: aResource.dataLength!)
                                                self.serviceCompletedFlag = false
                                            }
                                        }
                                    } else {
                                        self.serviceCompletedFlag = false
                                    }
                                } catch let error {
                                    print(error.localizedDescription)
                                    self.serviceCompletedFlag = false
                                }
                            } else {
                                //logging
                                self.textLog.write("Received Status code for a chunk other than 200")
                                //
                                self.serviceCompletedFlag = false
                            }
                            if (!self.serviceCompletedFlag && self.totalOprationCount == operationList.count) {
                                //print("........................... Recall Upload Chunk Operation ...........................")
                                //logging
                                self.textLog.write("Upload failed for some of the chunks or uri not received.")
                                //
                                self.serviceCompletedFlag = true
                                self.totalOprationCount = 0
                                DispatchQueue.main.async {
                                    self.delegate?.uploadCompletedForRecord(serviceRequestId: resource.serviceRequestId!, inspectorId: resource.inspectorId!, status: false)
                                }
                            }
                        }
                        operationList.append(uploadOperation)
    //                    operationQueue.addOperation(uploadOperation)
                    }
                    
                }
                //
                operationQueue.addOperations(operationList, waitUntilFinished: true)
            }
            else {      // all resources has been uploaded already
                DispatchQueue.main.async {
                    self.delegate?.uploadCompletedForRecord(serviceRequestId: self.serviceRequestId, inspectorId: self.inspectorId, status: true)
                }
            }
        }
        else {
            //print("Network not reachable")
            self.delegate?.uploadCompletedForRecord(serviceRequestId: "0", inspectorId: "0", status: false)
        }
    }
    
    func assessResources(resources: [ResourceMO]) {
        if CheckNetworkConnectivity().isNetworkAvailable() > 0 {
            var operationList = [UploadChunkResourceOperation]()
            var medias = [[String:Any]]()
            var serviceRequestId:String = "0"
            var inspectorId:String = "0"
            for resource in resources {
                var media = [String:Any]()
                media[kMediaPath] = resource.url
                media[kFileId] = resource.resourceId
                media[kMediaType] = resource.type
                media[kContainerName] = resource.containerName
                medias.append(media)
                serviceRequestId = resource.serviceRequestId!
                inspectorId = resource.inspectorId!
            }
//            print("Resource Dictionary:", medias)
            var blobDictionary = [String:Any]()
            blobDictionary[kServiceRequestId] = serviceRequestId
            blobDictionary[kInspectorId] = inspectorId
            blobDictionary[kMedia] = medias
            blobDictionary[kAppId] = kApplicationId
//            print("MEDIA Dictionary:", blobDictionary)
            //logging
            textLog.write(String(format: "Assessment Started: %@ \n Media Count to upload: %d \n Reource Dict Received To Upload : \n %@ \n", NSDate().description, medias.count, blobDictionary.description))
            //
            let uploadURL = UploadChunkResourceOperation(withParameters: blobDictionary)
            uploadURL.completionBlock = {
                if uploadURL.mediaResponseData != nil {
                    do {
                        let responseDictionary:NSDictionary = try JSONSerialization.jsonObject(with: uploadURL.mediaResponseData!, options: []) as! NSDictionary
                        //logging
                        self.textLog.write(String(format: "Assessment Response %@ \n", responseDictionary.description))
                        //
                        if responseDictionary[kIsSuccess] as! Bool {
//                            print("Resource Processing Response ::",responseDictionary)
                            self.delegate?.assessmentCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: true)
                        }
                        else {
                            //print("Error in Image Processing Operation ........")
                            self.delegate?.assessmentCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
                        }
                    } catch let error {
                        print(error.localizedDescription)
//                        print("Error in Json Prasing ...........")
                        self.delegate?.assessmentCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
                    }
                }
                else {
//                    print("Response is Nil..........")
                    self.delegate?.assessmentCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
                }
            }
            operationList.append(uploadURL)
            secondOperationQueue.addOperations(operationList, waitUntilFinished: true)
        }
        else {
            //print("Network not reachable........")
            //logging
            self.textLog.write("Network not reachable in Assessment request \n")
            //
            self.delegate?.assessmentCompletedForRecord(serviceRequestId: "0", inspectorId: "0", status: false)
        }
    }
    
    func bundlingResource(withserviceRequestId serviceRequestId: String, inspectorId:String, mediaCount:Int )->(){
        
        if CheckNetworkConnectivity().isNetworkAvailable() > 0 {
            
            var bundleDictionary = [String:Any]()
            bundleDictionary[kServiceRequestId] = serviceRequestId
            bundleDictionary[kInspectorId] = inspectorId
            bundleDictionary[kMeidaCount] = mediaCount
            bundleDictionary[kAppId] = kApplicationId
//            print("Bundle Dictionary:", bundleDictionary)
            
            let url = URL(string: kbundlingResourceURL)!
            let session = URLSession.shared
            var request = URLRequest(url: url)
            request.httpMethod = kHttpMethodPost
            do {
                request.httpBody = try JSONSerialization.data(withJSONObject: bundleDictionary, options: .prettyPrinted)
            } catch let error {
                print(error.localizedDescription)
            }
            request.addValue("application/json", forHTTPHeaderField: "Content-Type")
            request.addValue("application/json", forHTTPHeaderField: "Accept")
            request.addValue(kbundlingImageKeyValue, forHTTPHeaderField: ktokenName)
            print("bundle request :---------------", request)
            let task = session.dataTask(with: request as URLRequest, completionHandler: { data, response, error in
//                //print("Bundle Response:", response!)
                if error == nil {
                    if data != nil {
//                        let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
//                        print("Data:", responseString!)
                        if let httpResponse = response as? HTTPURLResponse {
                            if httpResponse.statusCode == 200 {
                                do {
                                    
                                    if let bundleResponse = try JSONSerialization.jsonObject(with: data!, options: .mutableContainers) as? [String: Any] {
//                                        print(bundleResponse[kSuccess] as! Bool)
                                        let isStatus = bundleResponse[kSuccess] as! Bool
                                        if isStatus {
                                            self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: true)
                                        }
                                        else {
                                            self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
                                        }
                                    }
                                } catch let error {
                                    print(error.localizedDescription)
                                    self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
                                }
                            }
                            else {
                                self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
                            }
                        }
                        else {
                            self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
                        }
                    }
                    else {
                        self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
                    }
                }
                else {
                    self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
                }
            })
            task.resume()
        }
        else {
            self.delegate?.bundlingCompletedForRecord(serviceRequestId: serviceRequestId, inspectorId: inspectorId, status: false)
        }
    }
    
    func uploadPercentageForAResource(resourceId: NSNumber) -> Int {
        let resource = self.dataController.fetchResourceDetail(resourceId: resourceId)
        let completePercentage: Int = ((resource.dataLength as! Int) - (resource.dataRemaining as! Int))*100/(resource.dataLength as! Int)
        return completePercentage
    }
    
    func uploadPercentageForARecord(serviceRequestId: String, inspectorId: String) -> Int {
        let resources = self.dataController.fetchResourcesForARecord(serviceRequestId: self.serviceRequestId, inspectorId: self.inspectorId)
        var totalData: Int = 0
        var remainingData: Int = 0
        for resource in resources {
            totalData = totalData + (resource.dataLength as! Int)
            remainingData = remainingData + (resource.dataRemaining as! Int)
        }
        let completePercentage: Int = (totalData - remainingData)*100/totalData
//        print("total : ",totalData)
//        print("remaining : ",remainingData)
//        print("percent : ",completePercentage)
        return (completePercentage <= 100) ? completePercentage : 100
    }
    
    func splitDatainChunks(_ resourcePath: URL) -> Array<Data> {
        var chunks:[Data] = [Data]()
        do
        {
            let data = try Data(contentsOf: resourcePath)
            let dataLen = (data as NSData).length
            let fullChunks = Int(dataLen / kChunkSize)
            let totalChunks = fullChunks + (dataLen % kChunkSize != 0 ? 1 : 0)
            for chunkCounter in 0..<totalChunks
            {
                var chunk:Data
                let chunkBase = chunkCounter * kChunkSize
                var diff = kChunkSize
                if chunkCounter == totalChunks - 1
                {
                    diff = dataLen - chunkBase
                }
                let range:Range<Data.Index> = Range<Data.Index>(chunkBase..<(chunkBase + diff))
                chunk = data.subdata(in: range)
                chunks.append(chunk)
            }
        }
        catch let error
        {
            print(error.localizedDescription)
        }
        return chunks;
    }

}

extension Data{
    mutating func append(_ string: String) {
        if let data = string.data(using: .utf8) {
            append(data)
        }
    }
}


