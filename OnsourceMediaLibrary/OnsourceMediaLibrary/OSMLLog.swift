//
//  OSMLLog.swift
//  OnsourceMediaLibrary
//
//  Created by Ankit Jain on 21/06/18.
//  Copyright © 2018 Ankit Jain. All rights reserved.
//

import Foundation

struct OSMLLog: TextOutputStream {
    
    var logFileName: String
    init(withLogFile fileName:String) {
        self.logFileName = String(format: "%@.txt", fileName)
    }
    
    /// Appends the given string to the stream.
    mutating func write(_ string: String) {
        let paths = FileManager.default.urls(for: .documentDirectory, in: .allDomainsMask)
        let documentDirectoryPath = paths.first!
        let log = documentDirectoryPath.appendingPathComponent(self.logFileName)
        
        do {
            let handle = try FileHandle(forWritingTo: log)
            handle.seekToEndOfFile()
            handle.write(string.data(using: .utf8)!)
            handle.closeFile()
        } catch {
            print(error.localizedDescription)
            do {
                try string.data(using: .utf8)?.write(to: log)
            } catch {
                print(error.localizedDescription)
            }
        }
        
    }
    
}
