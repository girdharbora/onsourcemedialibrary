//
//  OnsourceCoreDataUtility.swift
//  OnsourceMediaLibrary
//
//  Created by Ankit Jain on 07/02/18.
//  Copyright © 2018 Ankit Jain. All rights reserved.
//

import Foundation
import CoreData
import UIKit

 @objc class CoreDataController: NSObject {
    
    @objc static let shared = CoreDataController()
    
    @objc var managedObjectContext: NSManagedObjectContext
    var persistentStoreCoordinator: NSPersistentStoreCoordinator
    
    private override init() {
        guard let modelURL = Bundle.main.url(forResource: "OnsourceMediaLibrary", withExtension:"momd") else {
            fatalError("Error loading model from bundle")
        }
        
        guard let mom = NSManagedObjectModel(contentsOf: modelURL) else {
            fatalError("Error initializing mom from: \(modelURL)")
        }
        
        persistentStoreCoordinator = NSPersistentStoreCoordinator(managedObjectModel: mom)
        
        managedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = persistentStoreCoordinator
        
        guard let docURL = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).last else {
            fatalError("Unable to resolve document directory")
        }
        let storeURL = docURL.appendingPathComponent("OnsourceMediaLibrary.sqlite")
        do {
            try persistentStoreCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: storeURL, options: nil)
        } catch {
            fatalError("Error migrating store: \(error)")
        }
    }
    
    @objc func saveContext(context:NSManagedObjectContext) {
        let curManagedObjectContext:NSManagedObjectContext? = context;
//        curManagedObjectContext?.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
        
        if (curManagedObjectContext != nil) {
            curManagedObjectContext?.performAndWait({
                if (curManagedObjectContext?.hasChanges)! {
                    do {
                        try curManagedObjectContext?.save()
                        self.managedObjectContext.performAndWait {
                            do {
                                try self.managedObjectContext.save()
                            }
                            catch {
                                let nserror = error as NSError
                                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                            }
                        }
                    } catch {
                        // Replace this implementation with code to handle the error appropriately.
                        // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                        let nserror = error as NSError
                        fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
                    }
                }
            })
        }
    }

    func saveResource(path: String, status: String, serviceRequestId: String, inspectorId: String, type: String, userId: String, url:String, resDescription: String, containerName:String,timeStarted: NSDate, claimId:String, resourceTimeStamp:String, geolocation:String, notes: String, dataLength:NSNumber, dataRemaining:NSNumber) {
        let currentMOC = self.getCurrentContext()
        let entity = NSEntityDescription.entity(forEntityName: "Resource", in: currentMOC)
        let newResource = NSManagedObject(entity: entity!, insertInto: currentMOC)
        newResource.setValue(path, forKey: "path")
        newResource.setValue(status, forKey: "uploadStatus")
        newResource.setValue(serviceRequestId, forKey: "serviceRequestId")
        newResource.setValue(self.getMaxID(), forKey: "resourceId")
        newResource.setValue(inspectorId, forKey: "inspectorId")
        newResource.setValue(type, forKey: "type")
        newResource.setValue(userId, forKey: "userId")
        newResource.setValue(url, forKey: "url")
        newResource.setValue(resDescription, forKey: "resDescription")
        newResource.setValue(containerName, forKey: "containerName")
        newResource.setValue(timeStarted, forKey: "timeStarted")
        newResource.setValue(claimId, forKey: "claimId")
        newResource.setValue(resourceTimeStamp, forKey: "resourceTimeStamp")
        newResource.setValue(geolocation, forKey: "geolocation")
        newResource.setValue(notes, forKey: "notes")
        newResource.setValue(dataLength, forKey: "dataLength")
        newResource.setValue(dataRemaining, forKey: "dataRemaining")
        
        self.saveContext(context: currentMOC)
    }
    
    func getMaxID() -> NSNumber {
        let currentMOC = self.getCurrentContext()
        let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        let sortDescriptor = NSSortDescriptor(key: "resourceId", ascending: true)
        fetchRequest.sortDescriptors = [sortDescriptor]
        var newID = NSNumber(value: 0)
        do {
            let result = try currentMOC.fetch(fetchRequest) as! [ResourceMO]
            if result.count > 0 {
                newID = NSNumber(value: (result.last?.resourceId?.intValue)! + 1)
            }
        } catch let error as NSError {
            NSLog("Unresolved error \(error)")
        }
        return newID
    }

    func saveChunk(resourceId:NSNumber, status: String, chunkId: NSNumber, timeStarted: NSDate, chunkSize: Int) {
        let currentMOC = self.getCurrentContext()
        let entity = NSEntityDescription.entity(forEntityName: "Chunk", in: currentMOC)
        let newChunk = NSManagedObject(entity: entity!, insertInto: currentMOC)
        newChunk.setValue(resourceId, forKey: "resourceId")
        newChunk.setValue(status, forKey: "uploadStatus")
        newChunk.setValue(chunkId, forKey: "chunkId")
        newChunk.setValue(timeStarted, forKey: "timeStarted")
        newChunk.setValue(chunkSize, forKey: "chunkSize")
        self.saveContext(context: currentMOC)
    }
    
    func updateResourceUploadStatus(resourceId: NSNumber, status: String) -> Bool {
        let currentMOC = self.getCurrentContext()
        var statusUpdated: Bool = false
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "resourceId = %@", resourceId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try currentMOC.fetch(request) as! [ResourceMO]
            for data in result {
                if(data.url! != "") {
                    statusUpdated = true
                    data.setValue(status, forKey: "uploadStatus")
                    self.saveContext(context: currentMOC)
                }
            }
            
        } catch {
            
            //print("Failed")
        }
        return statusUpdated
    }
    func updateResourceUploadProgress(resourceId: NSNumber, dataRemaining: NSNumber) {
        let currentMOC = self.getCurrentContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "resourceId = %@", resourceId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try currentMOC.fetch(request) as! [ResourceMO]
            for data in result {
                data.setValue(dataRemaining, forKey: "dataRemaining")
                self.saveContext(context: currentMOC)
            }
            
        } catch {
            
            //print("Failed")
        }
    }
    
    func updateResourceURL(resourceId: NSNumber, url: String) {
        let currentMOC = self.getCurrentContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "resourceId = %@", resourceId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try currentMOC.fetch(request) as! [ResourceMO]
            for data in result {
                data.setValue(url, forKey: "url")
                self.saveContext(context: currentMOC)
            }
            
        } catch {
            
            //print("Failed")
        }
    }
    
    func updateChunkUploadStatus(chunkId: NSNumber, resourceId: NSNumber, status: String) -> Int {
        let currentMOC = self.getCurrentContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Chunk")
        request.predicate = NSPredicate(format: "chunkId = %@ && resourceId = %@", chunkId,resourceId)
        request.returnsObjectsAsFaults = false
        var chunkSize = kChunkSize
        do {
            let result = try currentMOC.fetch(request) as! [ChunkMO]
            for data in result {
                chunkSize = data.chunkSize as! Int
                data.setValue(status, forKey: "uploadStatus")
                self.saveContext(context: currentMOC)
            }
            
        } catch {
            
            //print("Failed")
        }
        return chunkSize
    }
    
    func fetchResourceDetail(resourceId: NSNumber) -> ResourceMO {
        let currentMOC = self.getCurrentContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "resourceId = %@", resourceId)
        request.returnsObjectsAsFaults = false
        var resourceObj:ResourceMO = ResourceMO(context: currentMOC)
        do {
            let result = try currentMOC.fetch(request) as! [ResourceMO]
            resourceObj = result.last!
        } catch {
            
            //print("Failed")
        }
        return resourceObj;
    }

    func fetchResourceUploadStatus(resourceId: NSNumber) -> String {
        let currentMOC = self.getCurrentContext()
        //upload status of a resource
        var uploadStatus = ""
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "resourceId = %@", resourceId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try currentMOC.fetch(request) as! [ResourceMO]
            for data in result {
                uploadStatus = data.value(forKey: "uploadStatus") as! String
                //print(uploadStatus)
            }
            
        } catch {
            
            //print("Failed")
        }
        return uploadStatus;
    }

    func fetchChunkUploadStatus(chunkId: NSNumber, resourceId: NSNumber) -> String {
        let currentMOC = self.getCurrentContext()
        //upload status of specific chunk of a resource
        var uploadStatus = ""
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Chunk")
        request.predicate = NSPredicate(format: "chunkId = %@ && resourceId = %@", chunkId,resourceId)
        request.returnsObjectsAsFaults = false
        do {
            let result = try currentMOC.fetch(request) as! [ChunkMO]
            for data in result {
                uploadStatus = data.value(forKey: "uploadStatus") as! String
                //print(uploadStatus)
            }
            
        } catch {
            
            //print("Failed")
        }
        return uploadStatus;
    }
    
    func fetchResourcesToBeUploaded(serviceRequestId: String, inspectorId: String) -> [ResourceMO] {
        let currentMOC = self.getCurrentContext()
        //upload status whether all chunks has been uploaded for a resource or not
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "(serviceRequestId = %@ AND inspectorId = %@ AND uploadStatus = %@ OR uploadStatus = %@)", serviceRequestId, inspectorId, UploadStatus.Started.rawValue, UploadStatus.InProgress.rawValue)
        request.returnsObjectsAsFaults = false
        var resources = [ResourceMO]()
        do {
            resources = try currentMOC.fetch(request) as! [ResourceMO]
        } catch {
            
            //print("Failed")
        }
        return resources;
    }
    
    func fetchResourcesWithURLForARecord(serviceRequestId: String, inspectorId: String) -> [ResourceMO] {
        let currentMOC = self.getCurrentContext()
        //upload status whether all chunks has been uploaded for a resource or not
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "(serviceRequestId = %@ AND inspectorId = %@ AND url != \"\")", serviceRequestId, inspectorId)
        request.returnsObjectsAsFaults = false
        var resources = [ResourceMO]()
        do {
            resources = try currentMOC.fetch(request) as! [ResourceMO]
        } catch {
            
            //print("Failed")
        }
        return resources;
    }

    func fetchChunksToBeUploaded(resourceId: NSNumber) -> [ChunkMO] {
        let currentMOC = self.getCurrentContext()
        //upload status whether all chunks has been uploaded for a resource or not
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Chunk")
        request.predicate = NSPredicate(format: "(resourceId = %@) AND (uploadStatus = %@ OR uploadStatus = %@)", resourceId, UploadStatus.Started.rawValue, UploadStatus.InProgress.rawValue)
        request.returnsObjectsAsFaults = false
        var chunks = [ChunkMO]()
        do {
            chunks = try currentMOC.fetch(request) as! [ChunkMO]
        } catch {
            
            //print("Failed")
        }
        return chunks;
    }
    
    func fetchRemainingResourcesForARecord(serviceRequestId: String, inspectorId: String) -> [ResourceMO] {
        let currentMOC = self.getCurrentContext()
        //upload status whether all chunks has been uploaded for a resource or not
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "(serviceRequestId = %@ AND inspectorId = %@ AND (uploadStatus = %@ or uploadStatus = %@))", serviceRequestId, inspectorId, UploadStatus.Started.rawValue, UploadStatus.InProgress.rawValue)
        request.returnsObjectsAsFaults = false
        var resources = [ResourceMO]()
        do {
            resources = try currentMOC.fetch(request) as! [ResourceMO]
        } catch {
            
            //print("Failed")
        }
        return resources;
    }
    
    func fetchResourcesForARecord(serviceRequestId: String, inspectorId: String) -> [ResourceMO] {
        let currentMOC = self.getCurrentContext()
        //upload status whether all chunks has been uploaded for a resource or not
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "(serviceRequestId = %@ AND inspectorId = %@)", serviceRequestId, inspectorId)
        request.returnsObjectsAsFaults = false
        var resources = [ResourceMO]()
        do {
            resources = try currentMOC.fetch(request) as! [ResourceMO]
        } catch {
            
            //print("Failed")
        }
        return resources;
    }
    
    func deleteChunks(resourceId: NSNumber) {
        let currentMOC = self.getCurrentContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Chunk")
        request.predicate = NSPredicate(format: "resourceId = %@", resourceId)
        request.returnsObjectsAsFaults = false
        let result = try? currentMOC.fetch(request) as! [ChunkMO]
        for data in result! {
            currentMOC.delete(data)
        }
        self.saveContext(context: currentMOC)
    }
    
    func deleteResources(serviceRequestId: String, inspectorId: String) {
        let currentMOC = self.getCurrentContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Resource")
        request.predicate = NSPredicate(format: "(serviceRequestId = %@ AND inspectorId = %@)", serviceRequestId, inspectorId)
        request.returnsObjectsAsFaults = false
        let result = try? currentMOC.fetch(request) as! [ResourceMO]
        for data in result! {
            self.deleteChunks(resourceId: data.resourceId!)
            currentMOC.delete(data)
        }
        self.saveContext(context: currentMOC)
    }
    
    func deleteExpiredChunks(expiryTime: NSDate) {
        let currentMOC = self.getCurrentContext()
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "Chunk")
        request.predicate = NSPredicate(format: "timeStarted <= %@", expiryTime)
        request.returnsObjectsAsFaults = false
        if let result = try? currentMOC.fetch(request) as! [ChunkMO] {
            //print("Result Count:", result.count)
            for data in result {
                currentMOC.delete(data)
            }
            self.saveContext(context: currentMOC)
        }
        
    }
    
    // Get the new context if the DB context is on a different thread...
    func getCurrentContext() -> NSManagedObjectContext {
        var curMOC:NSManagedObjectContext? = self.managedObjectContext
        let thisThread:Thread = Thread.current
        if thisThread == Thread.main {
            if curMOC != nil {
                return curMOC!
            }
            let coordinator:NSPersistentStoreCoordinator? = self.persistentStoreCoordinator
            if coordinator != nil {
                curMOC = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.mainQueueConcurrencyType)
                curMOC?.persistentStoreCoordinator = coordinator
            }
            return curMOC!
        }
        // if this is some other thread....
        // Get the current context from the same thread..
        var threadManagedObjectContext:NSManagedObjectContext? = thisThread.threadDictionary.object(forKey:"MOC_KEY") as? NSManagedObjectContext;
        // Return separate MOC for each new thread
        if threadManagedObjectContext != nil {
            return threadManagedObjectContext!;
        }
        
        let coordinator:NSPersistentStoreCoordinator? = self.persistentStoreCoordinator
        if coordinator != nil {
            threadManagedObjectContext = NSManagedObjectContext(concurrencyType: NSManagedObjectContextConcurrencyType.privateQueueConcurrencyType)
//            threadManagedObjectContext?.persistentStoreCoordinator = coordinator
            threadManagedObjectContext?.mergePolicy = NSMergeByPropertyObjectTrumpMergePolicy
            threadManagedObjectContext?.undoManager = nil
            threadManagedObjectContext?.parent = self.managedObjectContext
            thisThread.threadDictionary.setObject(threadManagedObjectContext!, forKey: "MOC_KEY" as NSCopying)
        }
        return threadManagedObjectContext!;
    }
    
//    func managedObjectDidSaveNotification(notification :Notification) {
//        let savedContext:NSManagedObjectContext = notification.object as! NSManagedObjectContext
//
//        if savedContext != self.managedObjectContext {
//            return;
//        }
//        let thisThread:Thread = Thread.current
//        let threadManagedObjectContext:NSManagedObjectContext? = thisThread.threadDictionary.object(forKey:"MOC_KEY") as? NSManagedObjectContext;
//        threadManagedObjectContext?.perform {
//            threadManagedObjectContext?.mergeChanges(fromContextDidSave: notification)
//        }
//    }
    
}
