//
//  ViewController.swift
//  OnsourceMediaLibrary
//
//  Created by Ankit Jain on 06/02/18.
//  Copyright © 2018 Ankit Jain. All rights reserved.
//

import UIKit
import CoreTelephony

class ViewController: UIViewController,OSMLDelegate {
    
    let resuploader = ResourceUploader()
    override func viewDidLoad() {
        super.viewDidLoad()
        //        let mediaResource :[Dictionary<String,String>] = [["filePath":"266694.jpg","fileType":"Image"],["filePath":"266694.jpg","fileType":"Image"]]
        //        prepareAndStartUpload()
        //        let calendar = Calendar.current
        //        let expiryTime: NSDate = calendar.date(byAdding: .minute, value: 28, to: Date())! as NSDate
        //
        //        print("Current Time:",NSDate())
        //        print("Expiry Time:",expiryTime)
        resuploader.delegate = self
    }
    
    
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func assesmentUploader(_ sender: UIButton) {
        let resuploader = ResourceUploader()
        resuploader.delegate = self
        //resuploader.startAssessment(serviceRequestId: "1006", inspectorId: "156")
        resuploader.startAssessment(serviceRequestId: "987262", inspectorId: "2146365197")
    }
    
    @IBAction func bundleResource(_ sender: UIButton) {
        let resuploader = ResourceUploader()
        resuploader.startBundling(withserviceRequestId:"1006", inspectorId: "156")
    }
    
    @IBAction func uploadMedia(_ sender: Any) {
        prepareAndStartUpload()
        //        DispatchQueue.global(qos: .userInitiated).async {
        //            self.prepareAndStartUpload()
        //        }
    }
    
    func prepareAndStartUpload() {
        //        let mediaResource :[Dictionary<String,String>] = [["filePath":"cover_9.jpeg","fileType":"img","ContainerName":"Room1","description":"Room1"],["filePath":"nature1.jpg","fileType":"img","ContainerName":"Room1", "description":"Room1"],["filePath":"nature2.jpg","fileType":"img","ContainerName":"Room1","description":"Room1"],["filePath":"nature3.jpg","fileType":"img", "ContainerName":"Room1", "description":"Room1"], ["filePath":"test_JSON","fileType":"metadata", "ContainerName":"Room1","description":""],["filePath":"tiger1.mp4","fileType":"vid","ContainerName":"CNT1", "description":""]]
        //        let mediaResource :[Dictionary<String,String>] = [["filePath":"nature1.jpg","fileType":"img","ContainerName":"Room1","description":""]]
        
//        let mediaResource :[Dictionary<String,String>] = [["filePath":"nature11.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature12.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature13.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature21.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature22.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature23.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature31.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature32.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature33.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature41.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature42.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature43.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature51.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature52.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature53.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature61.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature62.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature63.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature71.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature72.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature73.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature81.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature82.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature83.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature91.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature92.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature93.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature101.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature102.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature103.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature111.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature112.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature113.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature121.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature122.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature123.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature131.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature132.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature133.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature141.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature142.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature143.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature151.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature152.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature153.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature161.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature162.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature163.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature171.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature172.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature173.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature181.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature182.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature183.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""]]
        
                let mediaResource :[Dictionary<String,String>] = [["filePath":"nature1.jpg","fileType":"img","ContainerName":"CNT3","description":"Big Image", "resourceTimeStamp":"01-01-2018 12.02.14 am", "geolocation":"1234.786,1234.09876", "scopeofwork":"testing"], ["filePath":"nature2.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""],["filePath":"nature3.jpg","fileType":"img","ContainerName":"CNT3","description":"Small Image","resourceTimeStamp":"", "geolocation":"", "scopeofwork":""]]
        
        //        ,["filePath":"nature1.jpg","fileType":"img","ContainerName":"2146355193", "description":""],["filePath":"nature2.jpg","fileType":"img","ContainerName":"2146355193","description":""],["filePath":"nature3.jpg","fileType":"img", "ContainerName":"2146355193", "description":""]
        
        //        let mediaResource :[Dictionary<String,String>] = [["filePath":"appBg.jpeg","fileType":"img","ContainerName":"Room1","description":""]]
        
        let resources:NSDictionary = ["ServiceRequestId":"1079955", "UserId":"", "InspectorId":"990","MediaResource":mediaResource, "claimId":""]
        //        let resources:NSDictionary = ["ServiceRequestId":"975635", "UserId":"1", "InspectorId":"2146355193","MediaResource":mediaResource, "claimId":"2146355193"]
        //        print(resources)
        //975635/2146355193/2146355193/2146355193
        
        
        let jsonData = try? JSONSerialization.data(withJSONObject: resources, options: [])
        let jsonString = String(data: jsonData!, encoding: .utf8)!
        print(jsonString)
        
        
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .long
        print("Start Time:",formatter.string(from: currentDateTime))
        
        resuploader.delegate = self
        resuploader.prepareAndStartUpload(resources: resources, viewController: self)
    }
    
    //Delegate methods
    
    func uploadCompletedForResource(serviceRequestId: String, inspectorId: String, resourceUrl: String) {
        print("uploaded Resource Blob URL:", resourceUrl)
    }
    
    func mediaLibraryNetworkErrorHandler() {
        print("Network not rechability Error")
    }
    
    func uploadCompletedForRecord(serviceRequestId: String, inspectorId: String, status: Bool) {
        print("uploadCompletedForRecord Status:", status)
        let currentDateTime = Date()
        let formatter = DateFormatter()
        formatter.timeStyle = .medium
        formatter.dateStyle = .long
        print("End Time:",formatter.string(from: currentDateTime))
        if status {
            resuploader.startAssessment(serviceRequestId: serviceRequestId, inspectorId: inspectorId)
        }
    }
    
    func uploadProgressForRecord(serviceRequestId: String, inspectorId: String, progressPercentage: Int) {
        print("Progress percentage -- : ", progressPercentage)
    }
    
    func assessmentCompletedForRecord(serviceRequestId: String, inspectorId: String, status: Bool) {
        print("assessmentCompletedForRecord Status:", status)
        if status {
            resuploader.startBundling(withserviceRequestId:serviceRequestId, inspectorId: inspectorId)
        }
    }
    
    func bundlingCompletedForRecord(serviceRequestId: String, inspectorId: String, status: Bool) {
        print("bundlingCompletedForRecord Status:", status)
    }
}
