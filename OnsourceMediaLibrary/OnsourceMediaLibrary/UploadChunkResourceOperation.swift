//
//  UploadChunkResourceOperation.swift
//  SmartImageUploader
//
//  Created by Girdhar on 14/02/18.
//  Copyright © 2018 Girdhar. All rights reserved.
//

import Foundation
class UploadChunkResourceOperation: OnsourceOperation {
    var mediaParameters = [String: Any]()
    var mediaResponseData: Data?
    
    init(withParameters paramters: [String: Any]) {
        self.mediaParameters = paramters
    }
    
    override func main() {
        guard isCancelled == false else {
            finish(true)
            return
        }
        executing(true)
        uploadMediaResource()
    }
    
    func uploadMediaResource() {
        
        let url = URL(string: kresourceUploadURL)!
        let session = URLSession.shared
        var request = URLRequest(url: url)
        request.httpMethod = kHttpMethodPost
        do {
            request.httpBody = try JSONSerialization.data(withJSONObject: self.mediaParameters, options: .prettyPrinted)
        } catch let error {
            print(error.localizedDescription)
        }
        request.addValue("application/json", forHTTPHeaderField: "Content-Type")
        request.addValue("application/json", forHTTPHeaderField: "Accept")
        request.addValue(kprocessImageKeyValue, forHTTPHeaderField: ktokenName)
        //print("request 2:---------------", request)
        let task = session.dataTask(with:request as URLRequest, completionHandler: { data, response, error in
//            print("Response 2:", response!)
            if error == nil {
                if data != nil {
                    let responseString = NSString(data: data!, encoding: String.Encoding.utf8.rawValue)
                    print("Data:", responseString!)
                    if let httpResponse = response as? HTTPURLResponse {
                        if httpResponse.statusCode == 200 {
                            self.mediaResponseData = data
                        }
                        else {
                            //print("Response Status Code != 200")
                            self.mediaResponseData = nil
                        }
                    }
                    else {
                        self.mediaResponseData = nil
                    }
                }
                else {
//                    print("Data")
                    self.mediaResponseData = nil
                }
            }
            else {
                //print("Error:", error as Any)
                self.mediaResponseData = nil
            }
            self.executing(false)
            self.finish(true)
        })
        task.resume()
    }
}
